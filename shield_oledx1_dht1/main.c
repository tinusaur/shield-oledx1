/**
 * Shield-OLEDx1 - Testing scripts
 * @author Neven Boyanov
 * This is part of the Tinusaur/Shield-OLEDx1 project.
 * ----------------------------------------------------------------------------
 *  Copyright (c) 2023 Tinusaur (https://tinusaur.com). All rights reserved.
 *  Distributed as open source under the MIT License (see the LICENSE.txt file)
 *  Please, retain in your work a link to the Tinusaur project website.
 * ----------------------------------------------------------------------------
 * Source code available at: https://gitlab.com/tinusaur/shield-oledx1
 */

// ============================================================================

// NOTE: About F_CPU - it should be set in either (1) Makefile; or (2) in the IDE.

#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>

#include "tinyavrlib/cpufreq.h"

#include "tinudht/tinudht.h"

#include "ssd1306xled/ssd1306xled.h"
#include "ssd1306xled/ssd1306xledtx.h"
#include "ssd1306xled/font6x8.h"

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//                 ATtiny85
//               +----------+    (-)--GND--
//      (RST)--> + PB5  Vcc +----(+)--VCC--
// ---[OWOWOD]---+ PB3  PB2 +----[TWI/SCL]-
// --------------+ PB4  PB1 +--------------
// --------(-)---+ GND  PB0 +----[TWI/SDA]-
//               +----------+
//                 Tinusaur
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// NOTE: If you want to reassign the SCL and SDA pins and the I2C address
// do that in the library source code and recompile it so it will take affect.

// ----------------------------------------------------------------------------

#define TINUDHT_PIN PB1
#define TESTING_DELAY 500

int main(void) {

// ---- CPU Frequency Setup ----
#if F_CPU == 1000000UL
#pragma message "F_CPU=1MHZ"
	CLKPR_SET(CLKPR_1MHZ);
#elif F_CPU == 8000000UL
#pragma message "F_CPU=8MHZ"
	CLKPR_SET(CLKPR_8MHZ);
#else
#pragma message "F_CPU=????"
#error "CPU frequency should be either 1 MHz or 8 MHz"
#endif

	// ---- Initialization ----
	_delay_ms(40);	// Small delay might be necessary if ssd1306_init is the first operation in the application.
	ssd1306_init();
	ssd1306tx_init(ssd1306xled_font6x8data, ' ');
	// NOTE: Screen width - 128, that is 21 symbols per row (approximately).

	DDRB &= ~(1 << TINUDHT_PIN); // Make input.
	// PORTB |= (1 << TINUDHT_PIN); // Internal pull-up.

	ssd1306_clear();
	// ---- Fill out screen with patters ----
	ssd1306_fill2(0b00010100, 0b00101010);	// Dots
	_delay_ms(TESTING_DELAY);
	// ---- Print some text on the screen ----
	// 						   Fits:21chars ("123456789012345678901");
	ssd1306_setpos(4,  0);	ssd1306tx_string(" Shield OLEDx1 DEMO ");
	ssd1306_setpos(7,  2);	ssd1306tx_string(" DHT11: Temp & Hum ");
	ssd1306_setpos(7,  3);	ssd1306tx_string(" ----------------- ");
	ssd1306_setpos(22, 7);	ssd1306tx_string(" tinusaur.com ");
	_delay_ms(TESTING_DELAY);
	// ---- Main Loop ----
	for (;;) {
		TinuDHT tinudht;
		uint8_t tinudht_result = tinudht_read(&tinudht, TINUDHT_PIN);
		// ---- Print values on the screen ----
		if (tinudht_result == TINUDHT_OK) {
			ssd1306_setpos(7,  4);	ssd1306tx_string(" temperature=      ");
			ssd1306_setpos(84, 4);	ssd1306tx_numdecp(tinudht.temperature);
			ssd1306_setpos(7,  5);	ssd1306tx_string("    humidity=      ");
			ssd1306_setpos(84, 5);	ssd1306tx_numdecp(tinudht.humidity);
		} else {
			ssd1306_setpos(6,  4);
			ssd1306tx_string(" ERR: "); ssd1306tx_numdecp(tinudht_result);
			ssd1306_setpos(6,  5);	ssd1306tx_numdecp(tinudht.humidity); ssd1306tx_string(" "); ssd1306tx_numdecp(tinudht.temperature);
		}
		// IMPORTANT: Do not query the DHT11 more often than 1 time per second.
		_delay_ms(TESTING_DELAY << 2);
	}

	return 0; // Return the mandatory for the "main" function int value - "0" for success.
}

// ============================================================================
