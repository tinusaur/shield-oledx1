# Shield OLEDx1

Shield-OLEDx1 - Tinusaur Shield OLEDx1

    Copyright (c) 2021 Tinusaur (https://tinusaur.com). All rights reserved.
    Distributed as open source under the MIT License (see the LICENSE.txt file).
    Please, retain in your work a link to the Tinusaur project website.

- Shield-OLEDx1 source code:   https://gitlab.com/tinusaur/shield-oledx1  
  GitHub mirror at: https://github.com/tinusaur/shield-oledx1
- Shield-OLEDx1 project page:  https://tinusaur.com

NOTE: This project was developed for and tested on the ATtiny85 microcontrollers. It might or might not work in other environments.

- Tinusaur website: https://tinusaur.com
- Tinusaur on Twitter: https://twitter.com/tinusaur
- Tinusaur on Facebook: https://www.facebook.com/tinusaur

-------------------------------------------------------------------------------

Folders and modules:
- shield_oledx1	- The library
- shield_oledx1_dht1 - DHT11 and SSD1306xLED
- shield_oledx1_powsup1 - Power supply voltage and SSD1306xLED
- shield_oledx1_tempint1 - Internal temperature sensor and SSD1306xLED

